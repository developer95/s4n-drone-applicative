package se.application.core;

import se.application.core.business.ExecuteApplication;
import se.application.core.configuration.ProviderProperties;
import se.application.core.utils.Constants;

public class RunApplication {
    public static void main(String[] args) {
        ProviderProperties.load(Constants.PROPERTIES_DEFAULT_PATH);
        ExecuteApplication.run(ProviderProperties.getProperties());
    }
}
