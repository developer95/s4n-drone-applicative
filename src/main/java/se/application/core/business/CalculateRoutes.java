package se.application.core.business;

import se.application.core.dtos.ReadRouteDTO;
import se.application.core.dtos.ReportRouteDTO;

import java.util.List;

/**
 * @author davidgarcia
 */
public interface CalculateRoutes {
    List<ReportRouteDTO> perform(List<ReadRouteDTO> routes);
}
