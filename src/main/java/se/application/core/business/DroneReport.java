package se.application.core.business;

import se.application.core.dtos.ReportRouteDTO;
import se.application.core.utils.Constants;
import se.application.core.utils.Util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Class to create the documents with the report of the routes where the lunches were delivered.
 *
 * @author davidgarcia
 */
public class DroneReport {

    /**
     * Function for writer result routes reports.
     *
     * @param reportRouteDTO report source.
     * @param properties     of the system.
     */

    public static void writer(ReportRouteDTO reportRouteDTO, Properties properties) {
        try {
            final var directory = properties.getProperty(Constants.RESULT_ROUTES_DRONE_PATH);
            final var pathDirectory = Path.of(directory);
            final var fileName = "out" + Util.extractNumberFileName(reportRouteDTO.getNameFile()) + ".txt";
            final var pathCompleted = Path.of(String.join("/", directory, fileName));
            if (!Files.isDirectory(pathDirectory)) {
                Files.createDirectory(pathDirectory);
            }
            Files.deleteIfExists(pathCompleted);
            Files.createFile(pathCompleted);
            Files.writeString(pathCompleted, reportFormat(reportRouteDTO), StandardOpenOption.APPEND);
        } catch (Exception e) {
            System.out.println("Error writing file route: " + e.getMessage());
        }
    }

    /**
     * Function report format.
     *
     * @param reportRouteDTO report source.
     * @return string in the format.
     */
    private static String reportFormat(ReportRouteDTO reportRouteDTO) {
        return Constants.HEADER_REPORT.concat(
                reportRouteDTO.getDronePositionDTO()
                        .stream()
                        .map(dronePositionDTO -> ("(" + dronePositionDTO.getX() +
                                "," + dronePositionDTO.getY() + ")" + " " +
                                dronePositionDTO.getCompassDirection().getMessage() + "\n"))
                        .collect(Collectors.joining()));
    }
}
