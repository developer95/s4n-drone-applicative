package se.application.core.business;

import se.application.core.business.impl.CalculateRoutesImpl;

import java.util.Properties;

/**
 * Class to run the application
 *
 * @author davidgarcia
 */
public class ExecuteApplication {

    public ExecuteApplication() {
        throw new UnsupportedOperationException();
    }

    /**
     * Run application.
     *
     * @param properties of system.
     */
    public static void run(Properties properties) {
        System.out.println("..::: Start application homes lunches drone :::..");
        try {
            System.out.println("-> Reading drone routes from files ...");
            final var readDronesRoutes = new ReadDronesRoutes(properties);
            System.out.println("-> Obtaining delivery status of the routes and generating report in file out * .txt ...");
            final var calculateRoutes = new CalculateRoutesImpl(properties);
            calculateRoutes.perform(readDronesRoutes.readRoutesFiles());
            System.out.println("-> Process completed successfully");
            System.out.println("..::: End application :::..");
        } catch (Exception e) {
            System.out.println("Failure in the process: " + e.getMessage());
        }
    }
}
