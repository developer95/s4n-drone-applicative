package se.application.core.business;

import se.application.core.dtos.ReadRouteDTO;
import se.application.core.utils.Constants;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class to read the files with the drone routes and apply parameterized business logic.
 *
 * @author davidgarcia
 */
public class ReadDronesRoutes {

    private final String pathRoute;
    private final int limitLunches;
    private final int limitBlocks;
    private final Pattern countBlocksPattern;

    public ReadDronesRoutes(Properties properties) {
        this.pathRoute = properties.getProperty(Constants.ROUTES_DRONE_PATH).trim();
        this.limitLunches = Integer.parseInt(properties.getProperty(Constants.NUMBER_DRONE_LUNCHES).trim());
        this.limitBlocks = Integer.parseInt(properties.getProperty(Constants.NUMBER_BLOCKS.trim()));
        this.countBlocksPattern = Pattern.compile(Constants.ROUTE_BLOCKS_REGEX);

    }

    /**
     * Read routes files with business validations.
     *
     * @return List of routs and file name.
     * @throws IOException The exception is thrown when if there is an error accessing any file.
     */
    public List<ReadRouteDTO> readRoutesFiles() throws IOException {
        try (Stream<Path> paths = Files.list(Paths.get(pathRoute))
                .filter(path -> path.toString().matches(Constants.TEXT_ROUTES_REGEX))) {
            return paths
                    .map(path -> {
                        try {
                            return new ReadRouteDTO(path.getFileName().toString(), readAndFilterFileRoute(path, limitLunches, limitBlocks, countBlocksPattern));
                        } catch (IOException e) {
                            System.out.println("Failed read all lines route file: " + path.getRoot());
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Read and filter routes files.
     *
     * @param path               Route file.
     * @param limitLunches       Number of lunches carried by the drone.
     * @param limitBlocks        Limit of blocks around the neighborhood.
     * @param patternCountBlocks Regex for count number of blocks direction.
     * @return list of routes.
     * @throws IOException The exception is thrown when if there is an error accessing any file.
     */
    private List<String> readAndFilterFileRoute(Path path, int limitLunches, int limitBlocks, Pattern patternCountBlocks) throws IOException {
        return Files.readAllLines(path)
                .stream()
                .map(String::toUpperCase)
                .filter(s -> validationRoute(s, limitBlocks, patternCountBlocks))
                .limit(limitLunches)
                .collect(Collectors.toList());
    }

    /**
     * Validate input characters in route and number of blocks direction.
     *
     * @param route              For example "AAIDA"
     * @param limitBlocks        Parametric value.
     * @param patternCountBlocks Regex for count number of blocks direction.
     * @return Status of the validation if is true the validation is correct.
     */
    private boolean validationRoute(String route, int limitBlocks, Pattern patternCountBlocks) {
        final var blocks = patternCountBlocks.matcher(route).results().count();
        return route.matches(Constants.CHARACTERS_ROUTES_REGEX) && (blocks < limitBlocks);
    }
}
