package se.application.core.business.impl;

import se.application.core.business.CalculateRoutes;
import se.application.core.business.DroneReport;
import se.application.core.domain.DroneAxisPosition;
import se.application.core.dtos.DronePositionDTO;
import se.application.core.dtos.ReadRouteDTO;
import se.application.core.dtos.ReportRouteDTO;
import se.application.core.utils.CounterDirection;

import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Class for the calculation of drone routes and delivery report.
 *
 * @author davidgarcia
 */
public class CalculateRoutesImpl implements CalculateRoutes {
    private final Properties properties;

    public CalculateRoutesImpl(Properties properties) {
        this.properties = properties;
    }

    /**
     * Operation to calculate and report routes, writer the reports files.
     *
     * @param routes Delivery paths from file.
     * @return List with drone delivery report.
     */
    @Override
    public List<ReportRouteDTO> perform(List<ReadRouteDTO> routes) {
        return routes.parallelStream()
                .map(readRouteDTO -> new ReportRouteDTO(readRouteDTO.getNameFile(),
                        deliveriesDrone(readRouteDTO.getRoutes())))
                .peek(reportRouteDTO -> DroneReport.writer(reportRouteDTO, properties))
                .collect(Collectors.toList());
    }

    /**
     * Delivery route report by file.
     *
     * @param routes Lunch routes.
     * @return List of routes where drones delivered lunch.
     */
    private List<DronePositionDTO> deliveriesDrone(List<String> routes) {
        final var droneAxisPosition = DroneAxisPosition.getInstance();
        final var counterDirection = CounterDirection.getInstance();

        return routes.stream().map(s -> {
            positionDrone(droneAxisPosition, s.toCharArray(), counterDirection);
            return new DronePositionDTO(droneAxisPosition.getX(),
                    droneAxisPosition.getY(), droneAxisPosition.getCompassDirection());
        }).collect(Collectors.toList());
    }

    /**
     * Calculation the final delivery position.
     *
     * @param droneAxisPosition Drone position object.
     * @param routes            Lunch routes.
     * @param counterDirection  counter for direction changes.
     */
    private void positionDrone(DroneAxisPosition droneAxisPosition, char[] routes, CounterDirection counterDirection) {
        for (char route : routes) {
            switch (route) {
                case 'I':
                    counterDirection.decrement();
                    if (counterDirection.getValue() < -2) counterDirection.setValue(1);
                    droneAxisPosition.moveAndDirection(0, counterDirection.getValue());
                    break;
                case 'D':
                    counterDirection.increment();
                    if (counterDirection.getValue() > 1) counterDirection.setValue(-2);
                    droneAxisPosition.moveAndDirection(0, counterDirection.getValue());
                    break;
                case 'A':
                    droneAxisPosition.moveAndDirection(1, counterDirection.getValue());
                    break;
            }
        }

    }
}


