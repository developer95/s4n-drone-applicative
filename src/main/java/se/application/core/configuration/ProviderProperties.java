package se.application.core.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class that provides the external file properties.
 *
 * @author davidgarcia
 */
public final class ProviderProperties {
    private static final Properties properties = new Properties();

    private ProviderProperties() {
        throw new UnsupportedOperationException();
    }

    /**
     * Get properties.
     *
     * @return properties.
     */
    public static Properties getProperties() {
        return properties;
    }

    /**
     * Load  properties.
     *
     * @param path directory of properties.
     */
    public static void load(String path) {
        if (properties.isEmpty()) {
            try (InputStream inputStreamProperties = new FileInputStream(path)) {
                System.out.println("-> Loading properties");
                properties.load(inputStreamProperties);
            } catch (IOException e) {
                System.out.println("-> Fails loading properties " + e.getMessage());
            }
        }
    }
}


