package se.application.core.domain;

import static se.application.core.utils.Constants.*;

/**
 * Enum axis and messages.
 *
 * @author davidgarcia
 */
public enum CompassDirection {
    NORTH(NORTH_AXIS_VALUE), EAST(EAST_AXIS_VALUE), SOUTH(SOUTH_AXIS_VALUE), WEST(WEST_AXIS_VALUE);
    private final String message;

    CompassDirection(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
