package se.application.core.domain;

/**
 * Domain class  axis position drone.
 *
 * @author davidgarcia
 */
public class DroneAxisPosition {
    private int x;
    private int y;
    private CompassDirection compassDirection;

    {
        compassDirection = CompassDirection.NORTH;
    }

    public static DroneAxisPosition getInstance() {
        return new DroneAxisPosition();
    }

    public void moveY(int dy, CompassDirection compassDirection) {
        this.y += dy;
        this.compassDirection = compassDirection;
    }

    public void moveX(int dx, CompassDirection compassDirection) {
        this.x += dx;
        this.compassDirection = compassDirection;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public CompassDirection getCompassDirection() {
        return compassDirection;
    }


    /**
     * Move the drone by setting the number of movements and the direction.
     *
     * @param movement  Number of movements.
     * @param direction Axis: 0->NORTH, 1->EAST ,-1->WEST and -2->SOUTH.
     */
    public void moveAndDirection(int movement, int direction) {
        switch (direction) {
            case 0:
                this.moveY(movement, CompassDirection.NORTH);
                break;
            case 1:
                this.moveX(movement, CompassDirection.EAST);
                break;
            case -1:
                this.moveX((-1) * movement, CompassDirection.WEST);
                break;
            case -2:
                this.moveY((-1) * movement, CompassDirection.SOUTH);
                break;
        }
    }

    @Override
    public String toString() {
        return "DroneAxisPosition{" +
                "x=" + x +
                ", y=" + y +
                ", compassDirection=" + compassDirection +
                '}';
    }
}
