package se.application.core.dtos;

import se.application.core.domain.CompassDirection;

/**
 * Data transfer object class for position drone.
 *
 * @author davidgarcia
 */
public class DronePositionDTO {

    private final int x;
    private final int y;
    private final CompassDirection compassDirection;

    public DronePositionDTO(int x, int y, CompassDirection compassDirection) {
        this.x = x;
        this.y = y;
        this.compassDirection = compassDirection;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public CompassDirection getCompassDirection() {
        return compassDirection;
    }

    @Override
    public String toString() {
        return "DronePositionDTO{" +
                "x=" + x +
                ", y=" + y +
                ", compassDirection=" + compassDirection +
                '}';
    }
}
