package se.application.core.dtos;

import java.util.List;

/**
 * Data transfer object class for routes for drone.
 *
 * @author davidgarcia
 */
public final class ReadRouteDTO {
    private final String nameFile;
    private final List<String> routes;

    public ReadRouteDTO(String nameFile, List<String> routes) {
        this.nameFile = nameFile;
        this.routes = routes;
    }

    public String getNameFile() {
        return nameFile;
    }

    public List<String> getRoutes() {
        return routes;
    }

    @Override
    public String toString() {
        return "ReadRouteDTO{" +
                "nameFile='" + nameFile + '\'' +
                ", routes=" + routes +
                '}';
    }
}
