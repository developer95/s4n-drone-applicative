package se.application.core.dtos;

import java.util.List;

/**
 * Data transfer object class for report of route drone.
 *
 * @author davidgarcia
 */
public class ReportRouteDTO {
    private final String nameFile;
    private final List<DronePositionDTO> dronePositionDTO;

    public ReportRouteDTO(String nameFile, List<DronePositionDTO> dronePositionDTO) {
        this.nameFile = nameFile;
        this.dronePositionDTO = dronePositionDTO;
    }

    public String getNameFile() {
        return nameFile;
    }

    public List<DronePositionDTO> getDronePositionDTO() {
        return dronePositionDTO;
    }

    @Override
    public String toString() {
        return "ReportRouteDTO{" +
                "nameFile='" + nameFile + '\'' +
                ", dronePositionDTO=" + dronePositionDTO +
                '}';
    }
}
