package se.application.core.utils;

/**
 * Constants class for application logic.
 *
 * @author davidgarcia
 */
public class Constants {
    public static final String PROPERTIES_DEFAULT_PATH = "src/main/resources/configuration.properties";
    public static final String ROUTES_DRONE_PATH = "routes.drone.path";
    public static final String RESULT_ROUTES_DRONE_PATH = "result.routes.drone.path";
    public static final String NUMBER_DRONE_LUNCHES = "number.drone.lunches";
    public static final String NUMBER_BLOCKS = "number.blocks";
    public static final String CHARACTERS_ROUTES_REGEX = "^[ADI]*$";
    public static final String ROUTE_BLOCKS_REGEX = "[A]";
    public static final String TEXT_ROUTES_REGEX = ".*?\\.txt";
    public static final String EXTRACT_NUMBER_REGEX = "^\\D+(\\d+).*";
    public static final String NORTH_AXIS_VALUE = "dirección Norte";
    public static final String EAST_AXIS_VALUE = "dirección Oriente";
    public static final String WEST_AXIS_VALUE = "dirección Occidente";
    public static final String SOUTH_AXIS_VALUE = "dirección Sur";
    public static final String HEADER_REPORT = "== Reporte de entregas ==\n";

    private Constants() {
        throw new UnsupportedOperationException();
    }
}
