package se.application.core.utils;

/**
 * Class for count direction position.
 *
 * @author davidgarcia
 */
public class CounterDirection {
    private int value;

    public static CounterDirection getInstance() {
        return new CounterDirection();
    }

    public void increment() {
        this.value++;
    }

    public void decrement() {
        this.value--;
    }

    public int getValue() {
        return value;
    }

    public synchronized void setValue(int value) {
        this.value = value;
    }
}
