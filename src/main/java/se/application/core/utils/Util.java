package se.application.core.utils;

import java.util.regex.Pattern;

public class Util {
    private Util() {
        throw new UnsupportedOperationException();
    }

    /**
     * Extract number file name.
     *
     * @param nameRouteFile file name.
     * @return Number extracted.
     */

    public static String extractNumberFileName(String nameRouteFile) {
        final var pattern = Pattern.compile(Constants.EXTRACT_NUMBER_REGEX);
        final var matcher = pattern.matcher(nameRouteFile);
        if (matcher.find()) return matcher.group(1);
        else return "";
    }
}
