package se.application.core.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.application.core.configuration.ProviderProperties;
import se.application.core.domain.CompassDirection;
import se.application.core.dtos.DronePositionDTO;
import se.application.core.dtos.ReportRouteDTO;
import se.application.core.utils.Constants;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

class DronesReportsTest {
    private static final String PROPERTIES_PATH = "src/test/resources/test.properties";
    private static final String TEXT_NAME = "in01.txt";
    private static final int X_POSITION = 0;
    private static final int Y_POSITION = 0;
    private ReportRouteDTO reportRouteDTO;

    @BeforeEach
    void setUp() {

        final var dronePosition = new DronePositionDTO(X_POSITION, Y_POSITION, CompassDirection.NORTH);
        reportRouteDTO = new ReportRouteDTO(TEXT_NAME, Collections.singletonList(dronePosition));
        ProviderProperties.load(PROPERTIES_PATH);
    }

    @Test
    void writer() {
        final var directory = ProviderProperties.getProperties().getProperty(Constants.RESULT_ROUTES_DRONE_PATH);
        DroneReport.writer(reportRouteDTO, ProviderProperties.getProperties());
        final var path = Paths.get(String.join("/", directory, "out01.txt"));
        try {
            final var read = Files.readAllLines(path);
            Assertions.assertEquals("== Reporte de entregas ==", read.get(0));
            Assertions.assertEquals("(0,0) dirección Norte", read.get(1));
        } catch (IOException e) {
            System.out.println("Error run unit test: " + e.getMessage());
        }
    }
}