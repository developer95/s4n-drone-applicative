package se.application.core.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.application.core.configuration.ProviderProperties;

import java.io.IOException;

class ReadDronesRoutesTest {
    private static final String PROPERTIES_PATH = "src/test/resources/test.properties";
    private static final String FILE_NAME = "in01.txt";
    private static final String ROUTE_EXPECTED = "AIAAAADDA";

    private ReadDronesRoutes readDronesRoutes;

    @BeforeEach
    void setUp() {
        ProviderProperties.load(PROPERTIES_PATH);
        readDronesRoutes = new ReadDronesRoutes(ProviderProperties.getProperties());
    }

    @Test
    void readRoutesFiles() throws IOException {
        readDronesRoutes.readRoutesFiles()
                .stream()
                .filter(readRouteDTO -> readRouteDTO.getNameFile().equals(FILE_NAME))
                .flatMap(readRouteDTO -> readRouteDTO.getRoutes().stream())
                .findAny().ifPresent(s -> Assertions.assertEquals(ROUTE_EXPECTED, s));
    }
}