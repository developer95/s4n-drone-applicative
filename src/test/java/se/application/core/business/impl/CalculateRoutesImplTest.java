package se.application.core.business.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import se.application.core.business.CalculateRoutes;
import se.application.core.configuration.ProviderProperties;
import se.application.core.domain.CompassDirection;
import se.application.core.dtos.ReadRouteDTO;
import se.application.core.dtos.ReportRouteDTO;

import java.util.Arrays;
import java.util.List;

class CalculateRoutesImplTest {

    private static final String PROPERTIES_PATH = "src/test/resources/test.properties";
    private static final String TEXT_FILE_1 = "in01.txt";
    private static final String ROUTE_1_1 = "AAAAIAA"; // -2,4 NORTH
    private static final String ROUTE_1_2 = "DDDAIAD"; // -1,3 SOUTH
    private static final String ROUTE_1_3 = "AAIADAD"; // 0,0 WEST

    private static final String TEXT_FILE_2 = "in02.txt";
    private static final String ROUTE_2_1 = "AAAAAA"; // 0, 6 NORTH
    private static final String ROUTE_2_2 = "DDAAAA"; // 0, 2 SOUTH
    private static final String ROUTE_2_3 = "IIA";   //  0, 3 NORTH

    private CalculateRoutes calculateRoutes;
    private List<ReadRouteDTO> readRouteDTOS;

    @BeforeEach
    void setUp() {
        ProviderProperties.load(PROPERTIES_PATH);
        final var readRouteDTO1 = new ReadRouteDTO(TEXT_FILE_1, Arrays.asList(ROUTE_1_1, ROUTE_1_2, ROUTE_1_3));
        final var readRouteDTO2 = new ReadRouteDTO(TEXT_FILE_2, Arrays.asList(ROUTE_2_1, ROUTE_2_2, ROUTE_2_3));
        calculateRoutes = new CalculateRoutesImpl(ProviderProperties.getProperties());
        readRouteDTOS = Arrays.asList(readRouteDTO1, readRouteDTO2);
    }

    @Test
    void perform() {

        final var calculate = calculateRoutes.perform(readRouteDTOS);
        probeFinalRoute(calculate, TEXT_FILE_1, 0, 0, CompassDirection.WEST);
        probeFinalRoute(calculate, TEXT_FILE_2, 3, 0, CompassDirection.NORTH);

    }

    private void probeFinalRoute(List<ReportRouteDTO> reportRouteDTOS, String fileText, int y, int x, CompassDirection compassDirection) {
        reportRouteDTOS.stream()
                .filter(reportRouteDTO -> reportRouteDTO.getNameFile().equals(fileText))
                .flatMap(reportRouteDTO -> reportRouteDTO.getDronePositionDTO().stream())
                .skip(2)
                .findAny().ifPresent(dronePositionDTO -> {
                    Assertions.assertEquals(x, dronePositionDTO.getX());
                    Assertions.assertEquals(y, dronePositionDTO.getY());
                    Assertions.assertEquals(compassDirection, dronePositionDTO.getCompassDirection());
                }
        );
    }
}