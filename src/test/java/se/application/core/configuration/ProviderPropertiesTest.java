package se.application.core.configuration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProviderPropertiesTest {

    private static final String PROPERTIES_PATH = "src/test/resources/test.properties";
    private static final String PROPERTY = "test.value";
    private static final String PROPERTY_VALUE = "value";

    @BeforeEach
    void setUp() {
        ProviderProperties.load(PROPERTIES_PATH);
    }

    @Test
    void getProperties() {
        assertEquals(PROPERTY_VALUE, ProviderProperties.getProperties().getProperty(PROPERTY));
    }

}